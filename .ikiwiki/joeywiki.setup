# IkiWiki::Setup::Yaml - YAML formatted setup file
#
# Setup file for ikiwiki.
# 
# Passing this to ikiwiki --setup will make ikiwiki generate
# wrappers and build the wiki.
# 
# Remember to re-run ikiwiki --setup any time you edit this file.
#
# name of the wiki
wikiname: joey
# contact email for wiki
adminemail: joey@kitenet.net
# users who are wiki admins
adminuser:
  - http://joey.kitenet.net/
  - joey
# users who are banned from the wiki
banned_users: []
# where the source of the wiki is located
srcdir: /home/joey/src/joeywiki
# where to build the wiki
destdir: /home/joey/html
# base url to the wiki
url: http://localhost/~joey
# url to the ikiwiki.cgi
cgiurl: http://localhost/~joey/ikiwiki.cgi
# filename of cgi wrapper to generate
cgi_wrapper: /home/joey/html/ikiwiki.cgi
# mode for cgi_wrapper (can safely be made suid)
cgi_wrappermode: 06755
# rcs backend to use
rcs: git
# plugins to add to the default configuration
add_plugins:
  - emailauth
  - userlist
  - flattr
  - hnb
  - graphviz
  - testpagespec
  - filecheck
  - 404
  - remove
  - attachment
  - rename
  - listdirectives
  - theme
  - polygen
  - fortune
  - openid
  - poll
  - rawhtml
  - edittemplate
  - editdiff
  - recentchangesdiff
  - favicon
  - websetup
  - date
  - prettydate
  - relativedate
  - sparkline
  - linkmap
  - postsparkline
  - version
  - mirrorlist
  - otl
  - teximg
  - lockedit
  - comments
  - repolist
  - highlight
  - format
  - calendar
  - sidebar
  - aggregate
  - goodstuff
  - osm
  - opendiscussion
  - sidebar
  - graphviz
# plugins to disable
disable_plugins: []
# additional directory to search for template files
templatedir: /usr/share/ikiwiki/templates
# base wiki source location
underlaydir: /usr/share/ikiwiki/basewiki
# display verbose messages?
verbose: 1
# log to syslog?
#syslog: 1
# create output files named page/index.html?
usedirs: 1
# use '!'-prefixed preprocessor directives?
prefix_directives: 1
# use page/index.mdwn source files
indexpages: 0
# enable Discussion pages?
discussion: 1
# name of Discussion pages
discussionpage: Discussion
# generate HTML5? (experimental)
html5: 1
# only send cookies over SSL connections?
sslcookie: 0
# extension to use for new pages
default_pageext: mdwn
# extension to use for html files
htmlext: html
# strftime format string to display date
timeformat: '%a, %d %b %Y %H:%M:%S %z'
# UTF-8 locale to use
locale: en_US.UTF-8
# put user pages below specified page
userdir: ''
# how many backlinks to show before hiding excess (0 to show all)
numbacklinks: 10
# attempt to hardlink source files? (optimisation for large files)
hardlink: 1
only_committed_changes: 1
# force ikiwiki to use a particular umask
#umask: 022
# group for wrappers to run in
#wrappergroup: ikiwiki
# extra library and plugin directory
libdir: ''
# environment variables
ENV: {}
# regexp of normally excluded files to include
#include: '^\.htaccess$'
# regexp of files that should be skipped
#exclude: '^(*\.private|Makefile)$'
# specifies the characters that are allowed in source filenames
wiki_file_chars: '-[:alnum:]+/.:_'
# allow symlinks in the path leading to the srcdir (potentially insecure)
allow_symlinks_before_srcdir: 0

######################################################################
# core plugins
#   (branchable, editpage, git, htmlscrubber, inline, link, meta,
#    parentlinks)
######################################################################

# branchable plugin
# Allow anyone to branch, check out, and copy this site?
#branchable: 1
# Display "Branchable" link on action bar?
branchable_action: 1

# git plugin
# git hook to generate
#git_wrapper: /home/joey/src/joeywiki/.git/hooks/post-commit
# shell command for git_wrapper to run, in the background
#git_wrapper_background_command: git push github
# mode for git_wrapper (can safely be made suid)
git_wrappermode: 04755
# git pre-receive hook to generate
#git_test_receive_wrapper: /home/joey/src/joeywiki/.git/hooks/pre-receive
# unix users whose commits should be checked by the pre-receive hook
#untrusted_committers:
#  - joey
# gitweb url to show file history ([[file]] substituted)
historyurl: 'http://git.kitenet.net/?p=joey/joeywiki.git;a=history;f=[[file]];hb=HEAD'
# gitweb url to show a diff ([[file]], [[sha1_to]], [[sha1_from]], [[sha1_commit]], and [[sha1_parent]] substituted)
diffurl: 'http://git.kitenet.net/?p=joey/joeywiki.git;a=blobdiff;f=[[file]];h=[[sha1_to]];hp=[[sha1_from]];hb=[[sha1_commit]];hpb=[[sha1_parent]]'
# where to pull and push changes (set to empty string to disable)
gitorigin_branch: ''
# branch that the wiki is stored in
gitmaster_branch: master

# htmlscrubber plugin
# PageSpec specifying pages not to scrub
htmlscrubber_skip: '* and !comment(*)'

# inline plugin
# enable rss feeds by default?
rss: 1
# enable atom feeds by default?
atom: 0
# allow rss feeds to be used?
#allowrss: 0
# allow atom feeds to be used?
#allowatom: 0
# urls to ping (using XML-RPC) on feed update
pingurl: []

######################################################################
# auth plugins
#   (anonok, blogspam, httpauth, lockedit, moderatedcomments,
#    opendiscussion, openid, passwordauth, signinedit)
######################################################################

# anonok plugin
# PageSpec to limit which pages anonymous users can edit
anonok_pagespec: postcomment(*) or */discussion

# blogspam plugin
# PageSpec of pages to check for spam
blogspam_pagespec: postcomment(*)
# options to send to blogspam server
#blogspam_options: 'blacklist=1.2.3.4,blacklist=8.7.6.5,max-links=10'
# blogspam server XML-RPC url
#blogspam_server: ''

# httpauth plugin
# url to redirect to when authentication is needed
#cgiauthurl: http://localhost/~joey//auth/ikiwiki.cgi
# PageSpec of pages where only httpauth will be used for authentication
#httpauth_pagespec: '!*/Discussion'

# lockedit plugin
# PageSpec controlling which pages are locked
locked_pages: '* and !*/Discussion and !postcomment(*) and !sandbox'

# moderatedcomments plugin
# PageSpec matching users or comment locations to moderate
#moderate_pagespec: '*'

# openid plugin
# url pattern of openid realm (default is cgiurl)
#openid_realm: ''
# url to ikiwiki cgi to use for openid authentication (default is cgiurl)
#openid_cgiurl: ''

# passwordauth plugin
# a password that must be entered when signing up for an account
#account_creation_password: s3cr1t
# cost of generating a password using Authen::Passphrase::BlowfishCrypt
#password_cost: 8

######################################################################
# format plugins
#   (creole, highlight, hnb, html, mdwn, otl, po, rawhtml, rst, textile,
#    txt)
######################################################################

# highlight plugin
# types of source files to syntax highlight
#tohighlight: .c .h .cpp .pl .py Makefile:make
# location of highlight's filetypes.conf
filetypes_conf: /etc/highlight/filetypes.conf
# location of highlight's langDefs directory
langdefdir: /usr/share/highlight/langDefs

# mdwn plugin
# enable multimarkdown features?
#multimarkdown: 0

# po plugin
# master language (non-PO files)
#po_master_language: en|English
# slave languages (translated via PO files) format: ll|Langname
#po_slave_languages:
#  - fr|Français
#  - es|Español
#  - de|Deutsch
# PageSpec controlling which pages are translatable
#po_translatable_pages: '* and !*/Discussion'
# internal linking behavior (default/current/negotiated)
#po_link_to: current

######################################################################
# misc plugins
#   (filecheck)
######################################################################

######################################################################
# web plugins
#   (404, attachment, comments, editdiff, edittemplate, getsource, google,
#    goto, mirrorlist, remove, rename, repolist, search, theme, websetup,
#    wmd)
######################################################################

# attachment plugin
# enhanced PageSpec specifying what attachments are allowed
allowed_attachments: admin()
# virus checker program (reads STDIN, returns nonzero if virus found)
#virus_checker: clamdscan -

# comments plugin
# PageSpec of pages where comments are allowed
comments_pagespec: blog/entry/* and !*/discussion
# PageSpec of pages where posting new comments is not allowed
comments_closed_pagespec: ''
# Base name for comments, e.g. "comment_" for pages like "sandbox/comment_12"
comments_pagename: comment_
# Interpret directives in comments?
#comments_allowdirectives: 0
# Allow anonymous commenters to set an author name?
comments_allowauthor: 1
# commit comments to the VCS
comments_commit: 1

# getsource plugin
# Mime type for returned source.
#getsource_mimetype: text/plain; charset=utf-8

# mirrorlist plugin
# list of mirrors
mirrorlist:
  kite: http://kitenet.net/~joey

# repolist plugin
# URIs of repositories containing the wiki's source
repositories:
  - git://git.kitenet.net/joey/joeywiki
  - ssh://git.kitenet.net/srv/git/joey/joeywiki

# search plugin
# path to the omega cgi program
omega_cgi: /usr/lib/cgi-bin/omega/omega

# theme plugin
# name of theme to enable
#theme: actiontabs

# websetup plugin
# list of plugins that cannot be enabled/disabled via the web interface
#websetup_force_plugins: []
# list of additional setup field keys to treat as unsafe
websetup_unsafe: []
# show unsafe settings, read-only, in web interface?
websetup_show_unsafe: 1

######################################################################
# widget plugins
#   (calendar, color, conditional, cutpaste, date, format, fortune,
#    graphviz, haiku, img, linkmap, listdirectives, map, more, orphans,
#    pagecount, pagestats, poll, polygen, postsparkline, progress,
#    shortcut, sparkline, table, template, teximg, toc, toggle, version)
######################################################################

# calendar plugin
# base of the archives hierarchy
archivebase: blog/archives
# PageSpec of pages to include in the archives; used by ikiwiki-calendar command
archive_pagespec: blog/entry/* and !*/Discussion

# listdirectives plugin
# directory in srcdir that contains directive descriptions
directive_description_dir: ikiwiki/directive

# teximg plugin
# Should teximg use dvipng to render, or dvips and convert?
#teximg_dvipng: ''
# LaTeX prefix for teximg plugin
#teximg_prefix: |
#  \documentclass{article}
#  \usepackage[utf8]{inputenc}
#  \usepackage{amsmath}
#  \usepackage{amsfonts}
#  \usepackage{amssymb}
#  \pagestyle{empty}
#  \begin{document}
# LaTeX postfix for teximg plugin
#teximg_postfix: '\end{document}'

######################################################################
# other plugins
#   (aggregate, amazon_s3, autoindex, brokenlinks, camelcase, ddate, embed,
#    favicon, flattr, goodstuff, htmlbalance, ikiwikihosting, localstyle,
#    missingsite, pagetemplate, parked, pingee, pinger, prettydate,
#    recentchanges, recentchangesdiff, relativedate, rsync, sidebar,
#    smiley, sortnaturally, tag, testpagespec, transient, typography,
#    underlay)
######################################################################

# aggregate plugin
# enable aggregation to internal pages?
aggregateinternal: 1
# allow aggregation to be triggered via the web?
#aggregate_webtrigger: 0
# cookie control
#cookiejar:
#  file: /home/joey/.ikiwiki/cookies

# amazon_s3 plugin
# public access key id
#amazon_s3_key_id: XXXXXXXXXXXXXXXXXXXX
# file holding secret key (must not be readable by others!)
#amazon_s3_key_id: /home/joey/.s3_key
# globally unique name of bucket to store wiki in
#amazon_s3_bucket: mywiki
# a prefix to prepend to each page name
#amazon_s3_prefix: wiki/
# which S3 datacenter to use (leave blank for default)
#amazon_s3_location: EU
# store each index file twice? (allows urls ending in "/index.html" and "/")
#amazon_s3_dupindex: 0

# autoindex plugin
# commit autocreated index pages
#autoindex_commit: 1

# camelcase plugin
# list of words to not turn into links
#camelcase_ignore: []

# flattr plugin
# userid or user name to use by default for Flattr buttons
flattr_userid: joeyh

# ikiwikihosting plugin
# list of urls that alias to the main url
#urlalias: []
# openid of primary site owner
#owner: ''
# optional hostname of site this one was branched from
#parent: ''
# internal hostname of this site
#hostname: ''
# site creation datestamp
#created: ''
# how many days to retain logs
#log_period: 7
# disable IPv6?
#ipv6_disabled: 1

# parked plugin
# An optional message explaining why this site is parked.
#parked_message: ''

# pinger plugin
# how many seconds to try pinging before timing out
#pinger_timeout: 15

# prettydate plugin
# format to use to display date
prettydateformat: '%X, %B %o, %Y'

# recentchanges plugin
# name of the recentchanges page
recentchangespage: recentchanges
# number of changes to track
recentchangesnum: 100

# rsync plugin
# command to run to sync updated pages
#rsync_command: rsync -qa --delete . user@host:/path/to/docroot/

# sidebar plugin
# show sidebar page on all pages?
global_sidebars: 0

# tag plugin
# parent page tags are located under
#tagbase: tag
# autocreate new tag pages?
#tag_autocreate: 1
# commit autocreated tag pages
tag_autocreate_commit: 1

# typography plugin
# Text::Typography attributes value
#typographyattributes: 3

# underlay plugin
# extra underlay directories to add
#add_underlays:
#  - /home/joey/wiki.underlay
